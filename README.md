## FreeRun

    인간이 가장 많은 양의 정보를 얻는 시각정보만을 이용하여 자율주행을 구현해보고자 한다.

## Test video
 <a href="https://youtu.be/SS7KwGy5rSA" target="_blank"><img src="http://img.youtube.com/vi/SS7KwGy5rSA/0.jpg" width="360" height="240" border="10"/></a>

### Dependencies
* Raspberry Pi:
  - Picamera
* Computer:
  - Numpy 1.12.1
  - Opencv 3.1.0
  - Keras 2.1.3
  - Tensorflow-gpu 1.4.1
  - CUDA Toolkit 8.0
  - CuDNN v6.0
  
### About
- raspberrt_pi/ 
  -	***stream_client.py***: 비디오 프레임을 호스트 컴퓨터로 스트리밍한다.
  -	***control_client.py***: 전달받은 방향값에 따라 양 바퀴의 속도가 변화되도록 모터 드라이버로 명령을 전송한다.
- computer/
  -	training_data/ 
    - 신경망 학습을 위한 트레이닝 데이터가 npz 형식으로 저장된다.
  -	mlp_xml/ 
    - 학습된 mlp 신경망의 파라미터가 xml 형식으로 저장된다.
    - 본 프로젝트에서 트레이닝한 mlp 모델과 가중치가 제공된다.
  - nvidia/
    - 학습된 nvidia cnn 모델과 가중치가 각각 저장된다.
    - 본 프로젝트에서 트레이닝한 cnn 모델과 가중치가 제공된다.
  -	***collect_training_data.py***: 트레이닝 데이터로 사용하기 위해 스트리밍된 비디오 프레임을 받아 라벨링한다.
  -	***mlp_training.py***: mlp 신경망 학습
  - ***nvidia_model.ipynb***: nvidia cnn 모델 학습
  -	***rc_driver.py***: 비디오 프레임을 받아 RC car가 스스로 자율주행하도록 한다.

### How to drive
1. **Collect training data and testing data:** 먼저 ***collect_training_data.py***를 실행한다. 다음으로 라즈베리파이에서 ***controll_client.py***와 ***stream_client.py***를 순차적으로 실행한다. 사용자가 키보드(w, x, a, d)를 사용하여 RC car를 조종하면 비디오 프레임은 키보드가 눌렸을 때만 라벨링되어 저장된다. 주행을 마치고 트레이닝 데이터를 저장하려면 "q"를 눌러 프로그램을 종료한다. 데이터는 npz 형식으로 저장된다. 

2. **Neural network training:** mlp로 학습할 경우, ***mlp_training.py***를 실행한다. 학습을 마치면 모델은 ***mlp_xml*** 폴더에 저장된다. nvidia cnn 모델로 학습할 경우, ***nvidia_model.ipynb***를 실행한다. 학습을 마치면 모델과 가중치가 각각 ***nvidia*** 폴더에 저장된다.

3. **Self-driving in action**: 먼저 ***rc_driver.py***를 실행한다. 다음으로 라즈베리파이에서 ***controll_client.py***와 ***stream_client.py***를 순차적으로 실행한다.