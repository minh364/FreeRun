__author__ = 'zhengwang'

import cv2
import socket
import time
import os
import numpy as np
import keyboard

class CollectTrainingData(object):

    def __init__(self):

        self.server_socket = socket.socket()
        self.server_socket.bind(('192.168.0.90', 5001))
        self.server_socket.listen(0)
        # accept connection
        self.connection = self.server_socket.accept()[0].makefile('rb')

        # connect to send direction
        self.client_socket = socket.socket()
        self.client_socket.connect(('192.168.0.88', 50007))
        self.send_inst = True

        # create labels
        self.k = np.zeros((4, 4), 'float')
        for i in range(4):
            self.k[i, i] = 1
        self.temp_label = np.zeros((1, 4), 'float')

        self.collect_image()

    def collect_image(self):

        saved_frame = 0
        total_frame = 0

        # collect images for training
        print ('Start collecting images...')
        e1 = cv2.getTickCount()
        image_array = np.zeros((1, 120, 320))
        label_array = np.zeros((1, 4), 'float')

        # stream video frames one by one
        try:
            stream_bytes = ' '
            frame = 1
            while self.send_inst:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                direction = 's'
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)
                    # image = cv2.threshold(image, 150, 255, cv2.THRESH_BINARY)[1]

                    # select lower half of the image
                    roi = image[120:240, :]

                    # save streamed images
                    # cv2.imwrite('training_images/frame{:>05}.jpg'.format(frame), image)

                    # cv2.imshow('roi_image', roi)
                    cv2.imshow('image', image)

                    # reshape the roi image into one row array
                    temp_array = roi.reshape(1, 120, 320).astype(np.float32)

                    frame += 1
                    total_frame += 1

                    # get input from human driver
                    if keyboard.is_pressed('a'):
                        print("Left")
                        image_array = np.vstack((image_array, temp_array))
                        label_array = np.vstack((label_array, self.k[0]))
                        saved_frame += 1
                        direction = 'a'
                    elif keyboard.is_pressed('d'):
                        print("Right")
                        image_array = np.vstack((image_array, temp_array))
                        label_array = np.vstack((label_array, self.k[1]))
                        saved_frame += 1
                        direction = 'd'
                    elif keyboard.is_pressed('w'):
                        print("Forward")
                        saved_frame += 1
                        image_array = np.vstack((image_array, temp_array))
                        label_array = np.vstack((label_array, self.k[2]))
                        direction = 'w'
                    elif keyboard.is_pressed('x'):
                        print("Reverse")
                        saved_frame += 1
                        image_array = np.vstack((image_array, temp_array))
                        label_array = np.vstack((label_array, self.k[3]))
                        direction = 'x'
                    elif keyboard.is_pressed('s') or keyboard.KEY_UP:
                        print("Stop")
                        direction = 's'

                    if direction not in ['w', 'a', 's', 'd', 'x']:
                        continue

                    self.client_socket.sendall(direction.encode())

                if keyboard.is_pressed('q'):
                    print("Quit")
                    self.send_inst = False
                    # break

            # save training images and labels
            train = image_array[1:, :]
            train_labels = label_array[1:, :]
            print train
            print train_labels

            # save training data as a numpy file
            file_name = str(int(time.time()))
            directory = "training_data"
            if not os.path.exists(directory):
                os.makedirs(directory)
            try:
                np.savez(directory + '/' + file_name + '.npz', train=train, train_labels=train_labels)
            except IOError as e:
                print(e)

            e2 = cv2.getTickCount()
            # calculate streaming duration
            time0 = (e2 - e1) / cv2.getTickFrequency()
            print ('Streaming duration:', time0)

            print(train.shape)
            print(train_labels.shape)
            print ('Total frame:', total_frame)
            print ('Saved frame:', saved_frame)
            print ('Dropped frame', total_frame - saved_frame)

        finally:
            self.connection.close()
            self.server_socket.close()
            self.client_socket.close()


if __name__ == '__main__':
    CollectTrainingData()
