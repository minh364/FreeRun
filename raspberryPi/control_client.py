#!/usr/bin/env python

import socket
import wiringpi

# motor
STOP = 0
FORWARD = 1
BACKWORD = 2

# moter channel
CH1 = 0
CH2 = 1

# PIN input & output
OUTPUT = 1
INPUT = 0

# PIN setting
HIGH = 1
LOW = 0

# Raspberry GPIO setting
# PWM
ENA = 25
ENB = 30

# GPIO PIN
IN1 = 24
IN2 = 23
IN3 = 22
IN4 = 21

# PIN setting function
def setPinConfig(EN, INA, INB):
	wiringpi.pinMode(EN, OUTPUT)
	wiringpi.pinMode(INA, OUTPUT)
	wiringpi.pinMode(INB, OUTPUT)
	wiringpi.softPwmCreate(EN, 0, 255)

# motor control function
def setMotorControl(PWM, INA, INB, speed, stat):
	# motor speed control PWM
	wiringpi.softPwmWrite(PWM, speed)

	# FORWARD
	if stat == FORWARD:
		wiringpi.digitalWrite(INA, HIGH)
		wiringpi.digitalWrite(INB, LOW)

	# BACKWORD
	elif stat == BACKWORD:
		wiringpi.digitalWrite(INA, LOW)
		wiringpi.digitalWrite(INB, HIGH)

	# STOP
	elif stat == STOP:
		wiringpi.digitalWrite(INA, LOW)
		wiringpi.digitalWrite(INB, LOW)

# motor control function_wrap
def setMotor(ch, speed, stat):
	if ch == CH1:
		setMotorControl(ENA, IN1, IN2, speed, stat)

	else:
		setMotorControl(ENB, IN3, IN4, speed, stat)


# GPIO library setting
wiringpi.wiringPiSetup()

# motor PIN setting
setPinConfig(ENA, IN1, IN2)
setPinConfig(ENB, IN3, IN4)


HOST = '192.168.0.88'
PORT = 50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print ('Connected by', addr)

while 1:
	data = conn.recv(1024)
	if not data: break
	# decode_str = data.decode()


	if 'w' == data:
		print ('forward')
		setMotor(CH1, 150, BACKWORD)		
		setMotor(CH2, 150, FORWARD)

	elif 's' == data:
		print ('stop')
		setMotor(CH1, 150, STOP)		
		setMotor(CH2, 150, STOP)

	elif 'a' == data:
		print ('left')
		# left side reverse		
		#setMotor(CH1, 100, BACKWORD)		
		#setMotor(CH2, 100, BACKWORD)
		setMotor(CH1, 120, BACKWORD)		
		setMotor(CH2, 40, FORWARD)

	elif 'd' == data:
		print ('right')
		# right side reverse		
		# setMotor(CH1, 100, FORWARD)		
		# setMotor(CH2, 100, FORWARD)
		setMotor(CH1, 40, BACKWORD)		
		setMotor(CH2, 120, FORWARD)

	elif 'x' == data:
		print ('back')
		setMotor(CH1, 150, FORWARD)		
		setMotor(CH2, 150, BACKWORD)
	
	else:
		continue

	# conn.sendall(data)
conn.close()

